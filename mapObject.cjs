function mapObject(obj, cb) {
    if (arguments.length < 2 || (typeof obj !== 'object' && typeof obj !== 'string')) {
        return {};
    }

    let newObj = {};

    for (let key in obj) {
        newObj[key] = cb(obj[key]);
    }

    return newObj;
}

module.exports = mapObject;