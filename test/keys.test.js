const keys = require('../keys.cjs');

test('testing keys', () => {
    expect(keys({ one: 1, two: 2, three: 3 })).toStrictEqual(Object.keys({ one: 1, two: 2, three: 3 }))
})