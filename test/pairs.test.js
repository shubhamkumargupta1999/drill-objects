const pairs = require('../pairs.cjs');

test('testing pairs', () => {
    expect(pairs({ one: 1, two: 2, three: 3 })).toStrictEqual([["one", 1], ["two", 2], ["three", 3]])
})