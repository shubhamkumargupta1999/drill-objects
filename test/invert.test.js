const invert = require('../invert.cjs');

test('testing invert', () => {
    expect(invert({ Moe: "Moses", Larry: "Louis", Curly: "Jerome" })).toStrictEqual({ Moses: "Moe", Louis: "Larry", Jerome: "Curly" })
})