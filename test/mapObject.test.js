const mapObject = require('../mapObject.cjs');

test('testing mapObject', () => {
    expect(mapObject({ start: 5, end: 12 }, function (val, key) {
        return val + 5;
    })).toStrictEqual({ start: 10, end: 17 })
})