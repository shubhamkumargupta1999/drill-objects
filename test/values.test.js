const values = require('../values.cjs');

test('testing values', () => {
    expect(values({ one: 1, two: 2, three: 3 })).toStrictEqual(Object.values({ one: 1, two: 2, three: 3 }))
})