const defaults = require('../defaults.cjs');

test('testing defaults', () => {
    expect(defaults({ flavor: "chocolate" },
        { flavor: "vanilla", sprinkles: "lots" })).toStrictEqual({ flavor: "chocolate", sprinkles: "lots" })
})