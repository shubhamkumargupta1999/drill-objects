function defaults(obj, defaultObj) {

    if (arguments.length < 2
        || (typeof obj !== 'object' && typeof obj !== 'string')
        || (typeof defaultObj !== 'object' && typeof defaultObj !== 'string')) {

        return obj;
    }

    let newObj = { ...obj };

    for (let key in defaultObj) {
        if (newObj.hasOwnProperty(key) === false)
            newObj[key] = defaultObj[key];
    }

    return newObj;
}

module.exports = defaults;