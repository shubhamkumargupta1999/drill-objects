function invert(obj) {
    if (arguments.length < 1 || (typeof obj !== 'object' && typeof obj !== 'string')) {
        return {};
    }

    let newObj = {};

    for (let key in obj) {
        newObj[obj[key]] = key;
    }

    return newObj;
}

module.exports = invert;