function values(obj) {

    if (arguments.length < 1 || (typeof obj !== 'object' && typeof obj !== 'string')) {
        return [];
    }

    let arr = [];

    for (let key in obj) {
        arr.push(obj[key]);
    }

    return arr;
}

module.exports = values;