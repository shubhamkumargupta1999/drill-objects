function pairs(obj) {
    if (arguments.length < 1 || (typeof obj !== 'object' && typeof obj !== 'string')) {
        return [];
    }

    let arr = [];

    for (let key in obj) {
        arr.push([key, obj[key]]);
    }

    return arr;
}

module.exports = pairs;